from distutils.core import setup, Extension

soli_control_module = Extension('_soli_control', sources=['soli_control.i'], libraries = ['soli_core'], library_dirs = ['../lib'])
setup(name='soli_control', ext_modules=[soli_control_module], py_modules=["soli_control"])