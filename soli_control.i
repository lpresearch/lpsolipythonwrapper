# soli_control.i
# soli_control - Experimental SWIG wrapper definition file for Google Project Soli library 
# 2015-12-18 Klaus Petersen (klaus@lp-research.com)

%module soli_control
%{
#include "../include/accumulator.h"
#include "../include/capture.h"
#include "../include/classifier.h"
#include "../include/classlabelfilter.h"
#include "../include/controller.h"
#include "../include/corefeatures.h"
#include "../include/dataset.h"
#include "../include/datasetnode.h"
#include "../include/dsptransforms.h"
#include "../include/dtnode.h"
#include "../include/error.h"
#include "../include/fifobuffer.h"
#include "../include/fileio.h"
#include "../include/frame.h"
#include "../include/hysteresisthreshold.h"
#include "../include/kalmanfilter.h"
#include "../include/linearregression.h"
#include "../include/logisticregression.h"
#include "../include/movingaveragefilter.h"
#include "../include/object.h"
#include "../include/presencedetector.h"
#include "../include/radarsample.h"
#include "../include/radarsamplefileio.h"
#include "../include/randomforest.h"
#include "../include/rdfeatures.h"
#include "../include/regression.h"
#include "../include/resampler.h"
#include "../include/rffeatures.h"
#include "../include/rfpipeline.h"
#include "../include/ringbufferf.h"
#include "../include/ringbuffervf.h"
#include "../include/settings.h"
#include "../include/solistring.h"
#include "../include/temporalrbf.h"
#include "../include/util.h"
#include "../include/vector3.h"
#include "../include/vectorf.h"
#include "../include/vectori.h"
#include "../include/version.h"
#include "../include/wafe.h"
%}

%include "../include/accumulator.h"
%include "../include/capture.h"
%include "../include/classifier.h"
%include "../include/classlabelfilter.h"
%include "../include/controller.h"
%include "../include/corefeatures.h"
%include "../include/dataset.h"
%include "../include/datasetnode.h"
%include "../include/dsptransforms.h"
%include "../include/dtnode.h"
%include "../include/error.h"
%include "../include/fifobuffer.h"
%include "../include/fileio.h"
%include "../include/frame.h"
%include "../include/hysteresisthreshold.h"
%include "../include/kalmanfilter.h"
%include "../include/linearregression.h"
%include "../include/logisticregression.h"
%include "../include/movingaveragefilter.h"
%include "../include/object.h"
%include "../include/presencedetector.h"
%include "../include/radarsample.h"
%include "../include/radarsamplefileio.h"
%include "../include/randomforest.h"
%include "../include/rdfeatures.h"
%include "../include/regression.h"
%include "../include/resampler.h"
%include "../include/rffeatures.h"
%include "../include/rfpipeline.h"
%include "../include/ringbufferf.h"
%include "../include/ringbuffervf.h"
%include "../include/settings.h"
%include "../include/solistring.h"
%include "../include/temporalrbf.h"
%include "../include/util.h"
%include "../include/vector3.h"
%include "../include/vectorf.h"
%include "../include/vectori.h"
%include "../include/version.h"
%include "../include/wafe.h"

%{
static PyObject *my_pycallback = NULL;

static void PythonCallBack(Frame *frame)
{
	PyObject *func, *arglist;
	PyObject *result;

	swig_type_info* typeinfo = SWIG_TypeQuery("Frame *");
	func = my_pycallback;
	arglist = Py_BuildValue("()");
	PyObject* pyt =  SWIG_NewPointerObj(frame, typeinfo, 0); 
	arglist = Py_BuildValue("(O)", pyt); 
	result =  PyEval_CallObject(func, arglist);
	
	Py_DECREF(arglist);
	Py_XDECREF(result);
	
	return;
}

%}

%inline
%{

Controller *soli_wrapper_controller_create(PyObject *PyFunc)
{
	Py_XDECREF(my_pycallback);
	Py_XINCREF(PyFunc);
	
	my_pycallback = PyFunc;
	Controller *controller = soli_controller_create(PythonCallBack);
	
	return controller;
}

%}

%typemap(python, in) PyObject *PyFunc {
	if (!PyCallable_Check($input)) {
		PyErr_SetString(PyExc_TypeError, "Need a callable object!");
		return NULL;
	}
	
	$1 = $input;
}