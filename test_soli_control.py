# test_soli_control.py
# soli_control - project Soli C library experimental Python wrapper test
# 2015-12-18 Klaus Petersen (klaus@lp-research.com)

import soli_control as sc # Imports wrapper module
import time

# Callback gets called on new data
def test_callback(frame):
	# Prints out some sample data
	print "Frame ID:", sc.soli_frame_get_frame_id(frame)
	rs = sc.soli_frame_get_radar_sample(frame)
	print "Number of channels:", sc.soli_radar_sample_get_num_channels(rs)
	print "Channel 0 size:", sc.soli_radar_sample_get_sample_size(rs)
	cv = sc.soli_radar_sample_get_channel(rs, 0)
	print "First data value on channel 0:", sc.soli_vectorf_get(cv, 0)
	
# Instantiates the controller. NOTE: use wrapper function soli_wrapper_controller_create!
controller = sc.soli_wrapper_controller_create(test_callback)

# Same as in Soli examples:
sc.soli_controller_start(controller)
sc.soli_wait_for_key_press()
sc.soli_destroy(controller)

# Waits for controller thread to stop
time.sleep(1)