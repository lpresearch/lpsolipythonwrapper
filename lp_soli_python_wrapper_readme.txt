# lp_soli_python_wrapper_readme.txt
# Project Soli C library experimental Python wrapper install instructions
# 2015-12-18 Klaus Petersen (klaus@lp-research.com)

INSTALLATION

1. Download SWIG 3 from: http://www.swig.org/download.html
2. Extract to a new directory
3. Build and install SWIG by ./configure, ./make, ./make install
4. Clone Python wrapper repository to a subdirectory of your soli library eg. soli_library/LpSoliPythonWrapper/
5. Go to that directory
6. Do python setup.py build_ext --inplace
7. Do python test_soli_control.py
8. Copy output file _soli_control.so to your project source directory
9. Check test_soli_control.py on how to use the library


KNOWN PROBLEMS

The following headers aren't included yet:
matrixf.h
dspdata.h
microfeatures.h
timer.h

Script files needs to be modified. Feedback is welcome!
